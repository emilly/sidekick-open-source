class_name Level
extends Node2D

signal activate_possesion_vision
signal deactivate_possesion_vision
signal possesed_changed(possesed: Item)
signal player_died

@export var total_coins := 1
@export var light_gimmick := false
@export var currently_possesed: Node2D
@export var can_posses: Node2D

@onready var canvas_modulate := $CanvasModulate as CanvasModulate

@onready var coins = get_tree().get_nodes_in_group("Coins") as Array[Coin]

var tween: Tween

var coins_collected := 0

func _ready() -> void:
	for coin in coins:
		coin.collected.connect(_on_coin_collected)
	if light_gimmick:
		canvas_modulate.visible = true
	if currently_possesed:
		if currently_possesed.has_method("posses"):
			currently_possesed.posses(true)

func _on_coin_collected() -> void:
	coins_collected += 1

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("change_possesion"):
		stop_possesing()
	if event.is_action_pressed("reset"):
		reset()

func stop_possesing() -> void:
	if not has_children(can_posses):
		return
	if tween:
		tween.stop()
	Engine.time_scale = 0.05
	if currently_possesed:
		if currently_possesed.has_method("posses"):
			currently_possesed.posses(false)
	currently_possesed = null
	activate_possesion_vision.emit()
	possesed_changed.emit(currently_possesed)

func change_possesion(possesed: Node2D) -> void:
	Global.play_item_pickup()
	tween = get_tree().create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_QUINT)
	tween.tween_property(Engine, "time_scale", 1.0, 0.3)
	if currently_possesed:
		if currently_possesed.has_method("posses"):
			currently_possesed.posses(false)
	currently_possesed = possesed
	if currently_possesed:
		if currently_possesed.has_method("posses"):
			currently_possesed.posses(true)
	deactivate_possesion_vision.emit()
	possesed_changed.emit(currently_possesed)


var activated := false

func _on_level_complete_next_level() -> void:
	if not activated:
		activated = true
		Global.next_level(self)

func _on_level_complete_restart() -> void:
	get_tree().reload_current_scene()

# Bad.
# Ugly.
# Horrible.
# Atrocious.
# Horendous.
func has_children(node: Node2D) -> bool:
	var children := 0
	for child in node.get_children():
		if not child.is_queued_for_deletion() && child.can_posses:
			children = 1
			break
		else:
			continue
	return children

func _on_death_zone_body_entered(body: Node2D) -> void:
	if body is CharacterBody2D:
		body.die()

func reset() -> void:
	get_tree().reload_current_scene()
