extends ColorRect

@onready var level := get_tree().current_scene as Level

@onready var animation_player := $AnimationPlayer as AnimationPlayer

func _ready() -> void:
	color = Color(0, 0, 0, 0)
	level.activate_possesion_vision.connect(_on_activate_possesion_vision)
	level.deactivate_possesion_vision.connect(_on_deactivate_possesion_vision)

func _on_activate_possesion_vision() -> void:
	var tween := create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_SINE)
	tween.tween_property(self, "color", Color(0, 0, 0, 0.4), 0.3 * Engine.time_scale)
	tween.play()

func _on_deactivate_possesion_vision() -> void:
	var tween := create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_SINE)
	tween.tween_property(self, "color", Color(0, 0, 0, 0.0), 0.3)
	tween.play()
