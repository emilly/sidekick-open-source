extends CanvasLayer

signal next_level
signal restart

@onready var goals = get_tree().get_nodes_in_group("Goal") as Array[Goal]
@onready var coins = get_tree().get_nodes_in_group("Coins") as Array[Coin]
@onready var coins_count := coins.size()
@onready var coins_collected_label := %CoinsCollectedLabel

var coins_collected := 0

func _ready() -> void:
	for coin in coins:
		coin.collected.connect(_on_coin_collected)
	for goal in goals:
		goal.reached.connect(_on_goal_reached)
	
func _on_goal_reached() -> void:
	coins_collected_label.add_text("Coins Collected: %s/%s" % [coins_collected, coins_count])
	visible = true

func _on_coin_collected() -> void:
	coins_collected += 1


func _on_next_level_button_button_down() -> void:
	next_level.emit()
func _on_restart_button_button_down() -> void:
	restart.emit()
