extends Area2D

@export var collision: CollisionShape2D

@export var entity: PackedScene

@export var timer: Timer

func _ready() -> void:
	# R.I.P
	seed(7_11_1888)
	timer.timeout.connect(_on_timer_timeout)

func random_position() -> Vector2:
	var shape := collision.shape
	var size := shape.get_rect().size
	var random_x := randf_range(-size.x / 2, size.x / 2)
	var random_y := randf_range(-size.y / 2, size.y / 2)
	var random_vector := Vector2(random_x, random_y) 
	return random_vector

func generate_entity(pos: Vector2) -> void:
	var ent := entity.instantiate()
	ent.global_position = pos
	add_child(ent)

func _on_timer_timeout() -> void:
	generate_entity(random_position())
