extends CanvasLayer

@onready var player := get_tree().get_first_node_in_group("Player") as Player

func _ready() -> void:
	visible = true
	player.just_died.connect(_on_player_just_died)

func _on_player_just_died() -> void:
	$AnimationPlayer.play("play")
