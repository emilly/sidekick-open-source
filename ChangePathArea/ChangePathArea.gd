class_name ChangePathArea
extends Area2D

@export var path: Path2D

@export var one_shot := true

var reached := false

func _ready() -> void:
	body_entered.connect(_on_body_entered)

func _on_body_entered(body: Node2D) -> void:
	if one_shot && reached:
		return
	var player := body as Player
	if player:
		player.change_path(path)
		reached = true
