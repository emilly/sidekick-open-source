extends Sprite2D

var velocity: Vector2

func _ready() -> void:
	randomize()
	velocity = Vector2(randf_range(-0.06, 0.06), randf_range(-0.06, 0.06))

func _physics_process(delta: float) -> void:
	position = lerp(position, position + velocity, 0.1)
