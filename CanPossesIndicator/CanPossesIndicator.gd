extends Node2D

@onready var item := get_parent() as Item

@onready var level := get_tree().current_scene as Level

@onready var sprite := $Sprite2D as Sprite2D

var tween: Tween

var is_activated := false

func _ready() -> void:
	sprite.modulate = Color(1, 1, 1, 0)
	set_process_input(false)
	item.death.connect(_on_item_death)
	if item.can_posses:
		if level:
			level.activate_possesion_vision.connect(_on_activate_possesion_vision)
			level.deactivate_possesion_vision.connect(_on_deactivate_possesion_vision)

func _on_item_death() -> void:
	level.activate_possesion_vision.disconnect(_on_activate_possesion_vision)
	level.deactivate_possesion_vision.disconnect(_on_deactivate_possesion_vision)

func _on_activate_possesion_vision() -> void:
	set_process_input(true)
	is_activated = true
	activate()

func _on_deactivate_possesion_vision() -> void:
	set_process_input(false)
	is_activated = false
	deactivate()

func _on_area_2d_mouse_entered() -> void:
	if not item.can_posses:
		return
	if not is_activated:
		return
	if tween:
		tween.stop()
	tween = get_tree().create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_QUART)
	tween.tween_property(sprite, "scale", Vector2(2, 2), 0.3 * Engine.time_scale)
	tween.tween_property(sprite, "modulate", Color(1, 1, 1, 1), 0.3 * Engine.time_scale)
	tween.parallel()
	tween.play()

func _on_area_2d_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if not item.can_posses:
		return
	if event.is_action_pressed("select_possesion"):
		level.change_possesion(item)

func _on_area_2d_mouse_exited() -> void:
	if not is_activated:
		return
	if tween:
		tween.stop()
	tween = get_tree().create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_QUART)
	tween.tween_property(sprite, "scale", Vector2(1, 1), 0.3 * Engine.time_scale)
	tween.tween_property(sprite, "modulate", Color(1, 1, 1, 1.0), 0.3 * Engine.time_scale)
	tween.parallel()
	tween.play()

func deactivate() -> void:
	if tween:
		tween.stop()
	tween = get_tree().create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_QUART)
	tween.tween_property(sprite, "scale", Vector2(0, 0), 0.3 * Engine.time_scale)
	tween.tween_property(sprite, "modulate", Color(1, 1, 1, 0), 0.3 * Engine.time_scale)
	tween.parallel()
	tween.play()

func activate() -> void:
	if tween:
		tween.stop()
	tween = get_tree().create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_QUART)
	tween.tween_property(sprite, "scale", Vector2(1, 1), 0.1 * Engine.time_scale)
	tween.tween_property(sprite, "modulate", Color(1, 1, 1, 1.0), 0.3 * Engine.time_scale)
	tween.parallel()
	tween.play()
