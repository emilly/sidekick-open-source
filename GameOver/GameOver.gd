extends Control

@onready var secret_label := $VBoxContainer/SecretLabel as Label
@onready var coins_collected_label := $VBoxContainer/CoinsCollectedLabel as Label 

func _ready() -> void:
	if Global.current_coins == Global.max_coins:
		secret_label.visible = true
	else:
		secret_label.visible = false
	coins_collected_label.append_text("%s / %s" % [Global.current_coins, Global.max_coins])
