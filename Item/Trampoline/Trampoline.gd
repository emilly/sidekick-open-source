class_name Trampoline
extends Item

@export var power := Vector2(0, 300.0)

@onready var nearby: CharacterBody2D

func _physics_process(delta: float) -> void:
	if is_possesed && nearby:
		if Input.is_action_just_pressed("activate_special_ability"):
			Global.play_trampoline()
			nearby.velocity -= power.rotated(rotation)

func _on_trampoline_body_entered(body: Node2D) -> void:
	if body is CharacterBody2D:
		nearby = body

func _on_trampoline_body_exited(body: Node2D) -> void:
	if body is CharacterBody2D:
		nearby = null
