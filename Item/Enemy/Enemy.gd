class_name Enemy
extends Item

@export var movement_speed := 75.0

@export var gravity := 50.0

@onready var camera := get_tree().get_first_node_in_group("Camera") as Camera

@export var movement_direction := Vector2.LEFT
var previous_direction := Vector2.LEFT

func _physics_process(delta: float) -> void:
	if not is_possesed:
		if movement_direction.x == 0:
			movement_direction = previous_direction
		if is_on_wall():
			movement_direction.x = -movement_direction.x
			previous_direction.x = movement_direction.x
	if is_possesed:
		movement_direction = Vector2(
			int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left")),
			int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
		)
		if movement_direction != Vector2.ZERO:
			previous_direction = movement_direction
	velocity.x = lerp(velocity.x, movement_direction.x * movement_speed, 0.1)
	velocity.y = lerp(velocity.y, gravity, 0.1 * Engine.time_scale)
	move_and_slide()

func die() -> void:
	Global.play_death()
	camera.add_trauma(0.07)
	queue_free()

func _on_death_zone_body_entered(body: Node2D) -> void:
	if body is Player:
		body.die()
