class_name Platform
extends Item

@export var movement_speed := 100.0

var movement_direction := Vector2.ZERO

@export var vertical_movement := true
@export var horizontal_movement := false

@onready var elevator_music := $ElevatorMusic as AudioStreamPlayer2D

func _physics_process(delta: float) -> void:
	if not is_possesed:
		elevator_music.stop()
		return
	if not elevator_music.playing:
		elevator_music.play()
	if vertical_movement:
		movement_direction.y = -int(Input.is_action_pressed("move_up")) + int(Input.is_action_pressed("move_down"))
	if horizontal_movement:
		movement_direction.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	velocity = lerp(velocity, movement_direction * movement_speed, 0.1)
	move_and_slide()

func die() -> void:
	pass
