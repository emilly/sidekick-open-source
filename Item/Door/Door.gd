class_name Door
extends Item

var is_active := false

@onready var door := $Door

func _input(event: InputEvent) -> void:
	if is_possesed:
		if event.is_action_pressed("activate_special_ability"):
			activate()

func activate() -> void:
	Global.play_open_door()
	is_active = !is_active
	var tween := get_tree().create_tween()
	if is_active:
		tween.set_ease(Tween.EASE_OUT)
		tween.set_trans(Tween.TRANS_SINE)
		tween.tween_property(door, "position", Vector2(0, -16), .3)
	else:
		tween.set_ease(Tween.EASE_IN)
		tween.set_trans(Tween.TRANS_SINE)
		tween.tween_property(door, "position", Vector2(0, 0), .3)
	tween.play()
