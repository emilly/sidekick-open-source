class_name Coin
extends Item

signal collected
signal reached

@export var fake := false

@onready var level := get_tree().current_scene as Level

@onready var camera := get_tree().get_first_node_in_group("Camera") as Camera

@onready var collision := $CollisionShape2D as CollisionShape2D

var movement_velocity := Vector2.ZERO
var movement_direction := Vector2.ZERO
var movement_speed := 100.0
var gravity := 100.0
var jump_speed := 200.0

var activated := false

func _physics_process(delta: float) -> void:
	if is_possesed:
		movement_direction = Vector2(
			int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left")),
			int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
		)
	else:
		movement_direction = Vector2.ZERO
	if not is_jumping():
		if movement_direction.y < 0:
			velocity.y = -jump_speed
	velocity.x = lerp(velocity.x, movement_direction.x * movement_speed, 0.1 * Engine.time_scale)
	velocity.y = lerp(velocity.y, gravity, 0.1 * Engine.time_scale)
	move_and_slide()

func is_jumping() -> bool:
	if is_on_floor():
		return false
	return true 

func _on_pickup_body_entered(body: Node2D) -> void:
	if fake:
		if body is Player:
			if not activated:
				reached.emit()
				Global.play_level_end()
				activated = true
	else:
		if body is Player:
			set_collision_mask_value(2, 0)
			can_posses = false
			collected.emit()
			death.emit()
			Global.play_coin_pickup()
			collect()
			if is_possesed:
				level.stop_possesing()
		if body is Enemy:
			if is_possesed:
				level.stop_possesing()
			die()

func die() -> void:
	camera.add_trauma(0.1)
	Global.play_death()
	queue_free()

func collect() -> void:
	var tween := create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_SINE)
	tween.tween_property(self, "scale", Vector2.ZERO, 0.3)
	tween.play()
	await tween.finished
	queue_free()
