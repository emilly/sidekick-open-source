class_name FlyEnemy
extends Enemy

@onready var timer := $Timer as Timer

@onready var current_position := global_position

func _physics_process(delta: float) -> void:
	super(delta)
	if not is_possesed:
		if global_position.y > current_position.y:
			jump()
	if is_possesed:
		if Input.is_action_just_pressed("move_up"):
			jump()
		current_position = global_position

func jump() -> void:
	velocity.y -= 250
