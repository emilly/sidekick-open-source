class_name Item
extends CharacterBody2D

signal death

@export var is_possesed := false

@export var can_posses := true

# Q W E A S D
@export var key_control := [
	false, false, false,
	false, false, false,
] as Array[bool]

func posses(is_enabled: bool) -> void:
	is_possesed = is_enabled
