extends Area2D

@export var velocity := -100.0

@export var static_spikes := false

@onready var camera := get_tree().get_first_node_in_group("Camera")

func _ready() -> void:
	if not static_spikes:
		$GPUParticles2D.emitting = true

func _physics_process(delta: float) -> void:
	if not static_spikes:
		position.y += velocity * Engine.time_scale

func _on_body_entered(body: Node2D) -> void:
	if body is Coin:
		body.die()
	if not static_spikes:
		if body is TileMap:
			queue_free()
			return
		if body is Platform:
			camera.add_trauma(0.1)
			Global.play_death()
			queue_free()
			return
	if body is CharacterBody2D:
		body.die()
