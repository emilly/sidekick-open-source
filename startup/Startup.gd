extends Control

@export var next_scene: PackedScene

var once := true

func change_scene() -> void:
	once = true
	Global.next_level(null)

func _input(event: InputEvent) -> void:
	if once:
		if event.is_action_pressed("SkipStartup"):
			Global.next_level(null)
			once = false
