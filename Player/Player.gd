class_name Player
extends CharacterBody2D

signal just_died

@export var path: Path2D

@export var movement_speed := 50.0
@export var gravity := 200.0
@export var jump_speed := 200.0

@export var move_after_timer := false

@onready var spot_enemy := $SpotEnemy as RayCast2D
@onready var spot_pit := $SpotPit as RayCast2D

@onready var patrol_points := path.curve.get_baked_points()
@onready var patrol_index := 0

@onready var timer := get_tree().create_timer(3.0)

@onready var camera := get_tree().get_first_node_in_group("Camera")

var can_move := true

func _ready() -> void:
	if move_after_timer:
		can_move = false
	timer.timeout.connect(_on_timer_timeout)

func _physics_process(delta: float) -> void:
	spot_pit.force_raycast_update()
	spot_enemy.force_raycast_update()
	var target := Vector2.ZERO
	var movement_direction := Vector2.ZERO
	if path:
		target = patrol_points[patrol_index]
	if can_move:
		if path && global_position.distance_to(target) < 10:
			patrol_index += 1
			patrol_index = min(patrol_index, patrol_points.size() - 1)
			target = patrol_points[patrol_index]
			if patrol_index == patrol_points.size() - 1:
				path = null
				patrol_points = []
				patrol_index = 0
		movement_direction = Vector2.ZERO
		if path:
			movement_direction = (target - global_position).normalized()
		velocity.x = lerp(velocity.x, movement_direction.x * movement_speed, 0.1 * Engine.time_scale)
		velocity.y = lerp(velocity.y, gravity, 0.05 * Engine.time_scale)
		if not spot_pit.get_collider():
			jump()
		if spot_enemy.get_collider() is Enemy:
			jump()
	move_and_slide()
	if movement_direction.x != 0:
		Global.play_running()
	else:
		Global.stop_playing_running()

func jump() -> void:
	if is_on_floor():
		Global.play_jumping()
		velocity.y -= jump_speed

func die() -> void:
	can_move = false
	collision_layer = 0
	Global.play_death()
	camera.add_trauma(0.3)
	just_died.emit()
	Global.stop_playing_running()
	queue_free()

func _on_timer_timeout() -> void:
	if move_after_timer:
		can_move = true

func change_path(new_path: Path2D) -> void:
	path = new_path
	patrol_points = path.curve.get_baked_points()
