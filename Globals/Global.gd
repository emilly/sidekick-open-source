extends Node

@export var levels: Array[PackedScene]

@onready var music_player := $AudioStreamPlayer as AudioStreamPlayer
@onready var level_end := $LevelEnd as AudioStreamPlayer
@onready var item_pickup := $ItemPickup as AudioStreamPlayer

var has_music := true

@export var max_coins := 0

var current_coins := 0

func play_music() -> void:
	if has_music:
		music_player.play()

func stop_music() -> void:
	has_music = false
	music_player.stop()

func play_level_end() -> void:
	level_end.play()

func play_item_pickup() -> void:
	item_pickup.play()

func play_open_door() -> void:
	$DoorOpen.play()

func play_coin_pickup() -> void:
	$CoinPickup.play()

func play_jumping() -> void:
	$Jumping.play()

func play_running() -> void:
	if not $Footsteps.playing:
		$Footsteps.play()

func stop_playing_running() -> void:
	$Footsteps.stop()

func play_death() -> void:
	$Death.play()

func play_trampoline() -> void:
	$Trampoline.play()

func play_elevator() -> void:
	if not $Elevator.playing:
		$Elevator.play()

func stop_elevator() -> void:
	$Elevator.stop()

func next_level(previous_level: Level) -> void:
	if previous_level:
		max_coins += previous_level.total_coins
		current_coins += previous_level.coins_collected
	$AnimationPlayer.play("in")
	await $AnimationPlayer.animation_finished
	get_tree().change_scene_to_packed(levels.pop_front())
	get_tree().paused = true
	$AnimationPlayer.play_backwards("in")
	await $AnimationPlayer.animation_finished
	get_tree().paused = false
	if not music_player.playing:
		if levels.size() <= 1:
			return
		play_music()
