class_name MovementTutorial
extends Node2D

enum Keys {
	q = 0,
	w = 1,
	e = 2,
	a = 3,
	s = 4,
	d = 5
}

@onready var level := get_tree().current_scene as Level

@onready var keys := [
	$qTexture,
	$wTexture,
	$eTexture,
	$aTexture,
	$sTexture,
	$dTexture
] as Array[KeyControl]

func _input(event: InputEvent) -> void:
	if Input.is_action_pressed("activate_special_ability"):
		keys[Keys.e].activate()
	if Input.is_action_pressed("change_possesion"):
		keys[Keys.q].activate()
	if Input.is_action_pressed("move_up"):
		keys[Keys.w].activate()
	if Input.is_action_pressed("move_right"):
		keys[Keys.d].activate()
	if Input.is_action_pressed("move_left"):
		keys[Keys.a].activate()
	if Input.is_action_pressed("move_down"):
		keys[Keys.s].activate()
		
	if Input.is_action_just_released("activate_special_ability"):
		keys[Keys.e].deactivate()
	if Input.is_action_just_released("change_possesion"):
		keys[Keys.q].deactivate()
	if Input.is_action_just_released("move_up"):
		keys[Keys.w].deactivate()
	if Input.is_action_just_released("move_right"):
		keys[Keys.d].deactivate()
	if Input.is_action_just_released("move_left"):
		keys[Keys.a].deactivate()
	if Input.is_action_just_released("move_down"):
		keys[Keys.s].deactivate()

func _ready() -> void:
	level.possesed_changed.connect(_on_possesed_changed)
	if level.currently_possesed:
		var item := level.currently_possesed as Item
		disable_keys(item.key_control)
	else:
		disable_keys([true, false, false, false, false, false])

func disable_keys(key_control: Array[bool] = []) -> void:
	var index := 0
	for key in key_control:
		keys[index].disable(key)
		index += 1

func _on_possesed_changed(possesed: Item) -> void:
	if possesed:
		disable_keys(possesed.key_control)
	else:
		disable_keys([true, false, false, false, false, false])
