class_name KeyControl
extends Sprite2D

var tween: Tween

var time := 0.1
var ease_alg := Tween.EASE_IN_OUT
var trans_alg := Tween.TRANS_SINE

func activate() -> void:
	var tween = get_tree().create_tween().set_ease(ease_alg).set_trans(trans_alg)
	tween.tween_property(self, "scale", Vector2(1.25, 1.25), time * Engine.time_scale)
	tween.parallel()
	tween.play()

func deactivate() -> void:
	var tween = get_tree().create_tween().set_ease(ease_alg).set_trans(trans_alg)
	tween.tween_property(self, "scale", Vector2(1, 1), time * Engine.time_scale)
	tween.parallel()
	tween.play()

func disable(is_enabled: bool) -> void:
	if is_enabled:
		var tween = get_tree().create_tween().set_ease(ease_alg).set_trans(trans_alg)
		tween.tween_property(self, "scale", Vector2(1, 1), time)
		tween.tween_property(self, "modulate", Color(1, 1, 1, 1.0), time)
		tween.parallel()
		tween.play()
	else:
		var tween = get_tree().create_tween().set_ease(ease_alg).set_trans(trans_alg)
		tween.tween_property(self, "scale", Vector2(1, 1), time)
		tween.tween_property(self, "modulate", Color(1, 1, 1, 0.1), time)
		tween.parallel()
		tween.play()
