class_name Goal
extends Area2D

signal reached

var activated := false

func _on_body_entered(body: Node2D) -> void:
	if not activated:
		if body is Player:
			reached.emit()
			$GPUParticles2D.emitting = true
			activated = true
			Global.play_level_end()
