class_name PossesionTutorial
extends Control

@onready var level := get_tree().current_scene as Level

func _ready() -> void:
	visible = false
	level.activate_possesion_vision.connect(_on_activate_possesion_vision)
	level.deactivate_possesion_vision.connect(_on_deactivate_possesion_vision)

func _on_activate_possesion_vision() -> void:
	var tween := create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_QUART)
	tween.tween_property($Sprite2D, "scale", Vector2(2.5, 2.5), 0.5 * Engine.time_scale)
	tween.tween_property($Sprite2D, "scale", Vector2(2.0, 2.0), 0.5 * Engine.time_scale)
	tween.set_loops(-1)
	visible = true

func _on_deactivate_possesion_vision() -> void:
	visible = false
	queue_free()
